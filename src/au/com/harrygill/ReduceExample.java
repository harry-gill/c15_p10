package au.com.harrygill;

import java.util.stream.Stream;

public class ReduceExample {

    public static void main(String[] args) {

        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9).parallel();

        //Integer sum = integerStream.reduce(0, Integer::sum);
        //integerStream.reduce(Integer::sum).ifPresent(System.out::println);

        //System.out.println("Sum = " + sum);

        String concatenatedString = integerStream.reduce("", (a, b) -> a + b, (i, j) -> i + j);

        System.out.println(concatenatedString);
    }
}