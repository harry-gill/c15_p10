package au.com.harrygill;

import java.util.stream.Stream;

public class ReduceWithPrintEample {

    public static void main(String[] args) {

        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        /**
         * Uncomment the following lines and notice the value of a and b from the start to end.
         **/
        //        Integer sum = integerStream.reduce(0, (a, b) -> {
        //            System.out.print("a= " + a);
        //            System.out.println(" b= " + b);
        //            System.out.println("------------");
        //            return a + b;
        //        });
        //        System.out.println("Sum = " + sum);

        /**
         * Uncomment the following lines and notice the value of a and b from start to end
         * (note you will need to comment previous lines since we are using the same stream,
         * it cannot be consumed if consumed already.)
         */
        //        integerStream.reduce((a, b) -> {
        //            System.out.print("a= " + a);
        //            System.out.println(" b= " + b);
        //            System.out.println("------------");
        //            return a + b;
        //        }).ifPresent(System.out::println);


        /**
         * Uncomment the following lines and notice the value of a and b from start to end.
         * Since integerStream is not a parallel stream, you will notice that we will never print
         * i and j which means that the lambda we provided in third argument is never executed
         * for non-parallel streams. Also note that the thread id will be same for the whole program
         * (note you will need to comment previous lines since we are using the same stream,
         * it cannot be consumed if consumed already.)
         */
        //        String concatenatedString = integerStream.reduce("", (a, b) -> {
        //            System.out.println("Thread[" + Thread.currentThread().getId() + "]: " + "a = " + a);
        //            System.out.println("Thread[" + Thread.currentThread().getId() + "]: " + "b = " + b);
        //            return a + b;
        //        }, (i, j) -> {
        //            System.out.println("Thread[" + Thread.currentThread().getId() + "]: " + "i = " + i);
        //            System.out.println(" Thread[" + Thread.currentThread().getId() + "]: " + "j = " + j);
        //
        //            return i + j;
        //        });
        //        System.out.println("Output = " + concatenatedString);


        /**
         * Uncomment the following lines and notice the value of a and b from start to end.
         * Every new thread will have a = "" and b = next element. All these thread will run
         * in parallel and hence increasing the speed of the program.
         * Since integerParallelStream is a parallel stream, you will also notice that we will print
         * i and j this time. Also note that the thread id will not be same for the whole program.
         */
        //        Stream<Integer> integerParallelStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9).parallel();
        //
        //        String parallellyConcatenatedString = integerParallelStream.reduce("", (a, b) -> {
        //            System.out.println("Thread[" + Thread.currentThread().getId() + "]: " + "a = " + a);
        //            System.out.println("Thread[" + Thread.currentThread().getId() + "]: " + "b = " + b);
        //            return a + b;
        //        }, (i, j) -> {
        //            System.out.println("Thread[" + Thread.currentThread().getId() + "]: " + "i = " + i);
        //            System.out.println("Thread[" + Thread.currentThread().getId() + "]: " + "j = " + j);
        //            return i + j;
        //        });
        //        System.out.println("Output = " + parallellyConcatenatedString);

    }
}