package au.com.harrygill;

import java.util.List;

public class NonFunctionalReduceExample {

    public static void main(String[] args) {
        List<Integer> integers = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);

        Integer sum = 0;

        for (Integer i : integers) {
            sum = sum + i;
        }
        System.out.println("Sum = " + sum);
    }
}