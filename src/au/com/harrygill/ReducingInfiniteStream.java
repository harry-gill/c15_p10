package au.com.harrygill;

import java.util.stream.Stream;

public class ReducingInfiniteStream {

    public static void main(String[] args) {
        Stream<Integer> integerStream = Stream.generate(() -> 10);
        Integer sum = integerStream.reduce((a, b) -> a + b).get();
        System.out.println("Sum = " + sum);
    }
}